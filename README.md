## Tor Project - Community Policies

The Tor Project - Policies is the central repository for how we run the Tor Community. To uphold the Tor Project's value of being transparent, this repository is open to the world, and we welcome feedback. Please make a merge request to suggest improvements or add clarifications. To ask questions, use [issues](https://gitlab.torproject.org/tpo/community/policies/-/issues).

### Website version

For external public links, e.g., linking the Code of Conduct on new Job positions, please prefer the [web version](https://community.torproject.org/policies/) instead of the GitLab markdown file.
