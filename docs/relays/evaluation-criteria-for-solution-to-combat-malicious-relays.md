## Combating Malicious Relays - Evaluation criteria for solutions

As the Tor network is decentralized, diverse, and global, combating malicious relays is no small task, but it is critical. An important mechanism of increasing trust between Tor users, the Tor Project, and relay operators is creating a code of conduct and/or establishing behavior expectations with relays and exit node operators that have clear consequences. We plan to evaluate such mechanisms (e.g., some potential solutions include policies, agreements, and codes of conduct that are either legally enforceable or enforced by directory authorities), and implement selected solutions based on this evaluation.

Over the years, there were many potential solutions and non-formal agreements proposed. By developing this evaluation criteria, we aim to analyze proposals that can meet the Tor community’s expressed needs. The criteria below will help to evaluate and measure the success of such proposals like policies, agreements and methods of enforcement.

## Preamble

Any contributor who wants to improve the health of the network must adhere to the [Code of Conduct](../code_of_conduct.md) and Tor's [Social Contract](../social_contract.md) for their participation in shaping our policies and processes.

## Criteria

1. Solutions do not require any kind of prescreening for someone to participate
   in the Tor network.
    - There are no fees to be paid to participate in the network.
    - There is no moderation approval for running a relay in the first place.
    - Contributions to our network may happen regardless of sanctions against the
      country where an operator lives.
2. Solutions must meet the operator community's expressed needs.
    - Solutions need to adhere to the community-driven criteria which were
      identified by the operator community as “most critical”.
    - There is no substantial impact on the Tor network (no statistically
      significant change in Tor network performance) from non-malicious relays
      removed from the network by operators due to implemented solutions.
3. No solution may violate Tor's organization-wide Code of Conduct or Tor's
   Social Contract.
4. Solutions will accomplish their goals in the most rights preserving way
   possible.
    - Any decision to make operator rights/privacy trade-offs will be clearly
      documented and communicated to the operator community.
    - No solution will ask the operator for their legal name, passport information,
      physical addresses, phone numbers, or fotos.
5. Solutions, when taken together, will allow the Tor Project to more quickly
   identify and remove malicious relays.
6. The processes of dealing with potentially malicious activities work reliably
   and effectively
    - The processes are accurate and nondiscriminatory in detection methods.
    - The processes have notice and appeal mechanisms.
    - Investigations should be able to determine whether a claim of malicious
      activity is authentic and establish or disprove its veracity with sufficient
      accuracy.
7. Solutions must be done in a manner that ensures meaningful transparency and
   accountability.
    - Solutions must be publicly available, easy to find, and easy to understand.
    - Solutions which have consequences/enforcement must clearly describe what
      specific behaviors are not permitted and the consequences that a relay
      operator could face if they engage in those behaviors.
    - The process used to detect and investigate malicious activities should be
      clearly outlined.
8. Solutions that claim to be enforceable must be either legally enforceable or
   by technical means (for example, by directory authorities).
    - Operators should get a notice (e.g. in their Tor logs) after the enforcement
      and should have a point of contact to get more information about the
      underlying issue to potentially resolve it.
    - Enforcement actions must be publicly disclosed, either in an immediate and
      ongoing manner (for example, by tracking enforcement actions and/or
      investigations in a publicly available system), or through regular reporting
      (for example, by publishing evidence of enforcement actions in a quarterly
      transparency report).
9. There must be an appropriate level of adoption of any Solution that requires
   changes to relays to allow for those Solutions to be functional and meet
   other criteria above.
10. Solutions must respect human rights and privacy rights conventions.
    - Solutions must ensure that a range of perspectives and experiences are
      incorporated when creating or shaping them.
    - Solutions must not discriminate against anybody and must equally apply to
      everybody.
    - Solutions must not violate the dignity, due process and other human rights of
      good or malicious contributors.
